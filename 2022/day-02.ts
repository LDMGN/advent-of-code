import * as fs from 'fs';

/* Rock paper scissors. */

const rock = 'rock';
const paper = 'paper';
const scissors = 'scissors';

type RockPaperScissors = typeof rock | typeof paper | typeof scissors;

class TurnChoice {
    constructor(public readonly type: RockPaperScissors,
                public readonly beatsType: RockPaperScissors,
                public readonly losesToType: RockPaperScissors) {
    }

    public beats(other: TurnChoice): boolean {
        return other.type === this.beatsType;
    }
}

const rockChoice = new TurnChoice(rock, scissors, paper);
const paperChoice = new TurnChoice(paper, rock, scissors);
const scissorsChoice = new TurnChoice(scissors, paper, rock);

const typeToChoiceMap = {
    [rock]: rockChoice,
    [paper]: paperChoice,
    [scissors]: scissorsChoice,
};

const typeToChoice = (type: RockPaperScissors): TurnChoice => typeToChoiceMap[type];

interface Turn {
    opponentChoice: TurnChoice;
    playerChoice: TurnChoice;
}

/* Opponent encoding. */
const opponentRock = 'A';
type OpponentRock = 'A';

const opponentPaper = 'B';
type OpponentPaper = 'B';

const opponentScissors = 'C';
type OpponentScissors = 'C';

type OpponentChoice = OpponentRock | OpponentPaper | OpponentScissors;
const isOpponentChoice = (input: unknown): input is OpponentChoice =>
    input === opponentRock
    || input === opponentPaper
    || input === opponentScissors;

const turnChoiceFromOpponent = (input: OpponentChoice): TurnChoice => {
    switch (input) {
        case opponentRock:
            return rockChoice;
        case opponentPaper:
            return paperChoice;
        case opponentScissors:
            return scissorsChoice;
    }
};

/* Player encoding. */
type PlayerRock = 'X';
const playerRock = 'X';
const playerShouldLose = 'X';

type PlayerPaper = 'Y';
const playerPaper = 'Y';
const playerShouldDraw = 'Y';

type PlayerScissors = 'Z';
const playerScissors = 'Z';
const playerShouldWin = 'Z';

type PlayerChoice = PlayerRock | PlayerPaper | PlayerScissors;
const isPlayerChoice = (input: unknown): input is PlayerChoice =>
    input === playerRock
    || input === playerPaper
    || input === playerScissors;

const turnChoiceFromPlayer = (input: PlayerChoice): TurnChoice => {
    switch (input) {
        case playerRock:
            return rockChoice;
        case playerPaper:
            return paperChoice;
        case playerScissors:
            return scissorsChoice;
    }
};

/* Scoring. */
const rockScore = 1;
const paperScore = 2;
const scissorsScore = 3;

const scoreLoss = 0;
const scoreDraw = 3;
const scoreWin = 6;

const readFile = (fileName: string): string[] => {
    return fs.readFileSync(fileName)
        .toString('utf-8')
        .split('\r\n')
        .filter((line: string) => !!line);
};

type TurnCalculator = (opponentChoice: OpponentChoice,
                       playerInput: PlayerChoice) => TurnChoice;

const turnsFrom = (lines: string[],
                   playerChoiceCalculator: TurnCalculator): Turn[] =>
    lines
        .map((line: string): Turn => {
            const parts = line.split(' ');
            if (parts.length !== 2) {
                console.error('Invalid input line', line);
                throw new Error('Invalid input line');
            }

            const opponentChoice = parts[0];
            const playerChoice = parts[1];

            if (!isOpponentChoice(opponentChoice) || !isPlayerChoice(playerChoice)) {
                console.error('Invalid input line', line);
                throw new Error('Invalid input line');
            }

            return {
                opponentChoice: turnChoiceFromOpponent(opponentChoice),
                playerChoice: playerChoiceCalculator(opponentChoice, playerChoice),
            };
        });

const choiceScore = (choice: TurnChoice) => {
    switch (choice.type) {
        case rock:
            return rockScore;
        case paper:
            return paperScore;
        case scissors:
            return scissorsScore;
    }
    throw new Error('Invalid choice');
};

const resultScore = (turn: Turn): number => {
    if (turn.playerChoice === turn.opponentChoice) {
        return scoreDraw;
    }

    if (turn.playerChoice.beats(turn.opponentChoice)) {
        return scoreWin;
    }
    return scoreLoss;
};

const scoreTurn = (turn: Turn): number =>
    choiceScore(turn.playerChoice) + resultScore(turn);

const scoreTurns = (turns: Turn[]) =>
    turns
        .map(turn => scoreTurn(turn))
        .reduce((a: number, b: number) => a + b, 0);

const partOne = (fileName: string) =>
    scoreTurns(
        turnsFrom(
            readFile(fileName),
            (_, a) => turnChoiceFromPlayer(a)
        )
    );

console.log('--- Part 1 ---');

console.log(partOne('day-02-example.txt'));

console.log(partOne('day-02-input.txt'));

console.log('--- Part 2 ---');

const partTwo = (fileName: string) =>
    scoreTurns(
        turnsFrom(
            readFile(fileName),
            (opponentChoice: OpponentChoice,
             playerInput: PlayerChoice): TurnChoice => {
                if (playerInput === playerShouldWin) {
                    return typeToChoice(turnChoiceFromOpponent(opponentChoice).losesToType);
                }

                if (playerInput === playerShouldDraw) {
                    return typeToChoice(turnChoiceFromOpponent(opponentChoice).type);
                }

                return typeToChoice(turnChoiceFromOpponent(opponentChoice).beatsType);
            }
        )
    );

console.log(partTwo('day-02-example.txt'));

console.log(partTwo('day-02-input.txt'));
