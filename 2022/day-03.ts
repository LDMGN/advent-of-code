import * as fs from 'fs';

type Rucksack = {
    items: string[];
    firstCompartment: string[];
    secondCompartment: string[];
}

type ElfGroup = {
    rucksacks: Rucksack[];
}

const readFile = (fileName: string): string[] =>
    fs.readFileSync(fileName)
        .toString('utf-8')
        .split('\r\n')
        .filter((line: string) => !!line);

const splitRucksacks = (rucksacks: string[]): Rucksack[] =>
    rucksacks
        .map((rucksack: string) => {
            const items: string[] = rucksack.split('');

            if (items.length % 2 !== 0) {
                throw new Error('Uneven number of items in rucksack: ' + rucksack);
            }

            const middleIndex = items.length / 2;

            return {
                items,
                firstCompartment: items.slice(0, middleIndex),
                secondCompartment: items.slice(middleIndex),
            };
        });

const findDuplicatesInCompartments = (rucksacks: Rucksack[]): string[][] =>
    rucksacks
        .map((rucksack: Rucksack) => rucksack.firstCompartment
            .filter((item: string, index: number, list: string[]) => list.indexOf(item) === index)
            .filter((item) => rucksack.secondCompartment.includes(item)));

const priorityALower = 'a'.charCodeAt(0);
const priorityAUpper = 'A'.charCodeAt(0);
const priorityFromItem = (item: string): number => {
    const itemCode = item.charCodeAt(0);

    if (itemCode >= priorityALower) {
        return itemCode - priorityALower + 1;
    }

    if (itemCode >= priorityAUpper) {
        return itemCode - priorityAUpper + 1 + 26;
    }

    throw new Error('Unexpected code range');
};

const convertToPriorities = (errors: string[][]) => {
    return errors
        .map((rucksackErrors: string[]) => {
            return rucksackErrors
                .map((error: string) => priorityFromItem(error));
        });
};

const sumNumbers = (list: number[]) =>
    list.reduce((a, b) => a + b, 0);

const flatten = <T>(list: T[][]): T[] =>
    list
        .reduce((a, b) => [
                ...a,
                ...b
            ],
            []);

const partOne = (fileName: string) =>
    sumNumbers(
        flatten(
            convertToPriorities(
                findDuplicatesInCompartments(
                    splitRucksacks(
                        readFile(fileName)
                    )
                )
            )
        )
    );

const groupRucksacks = (rucksacks: Rucksack[]): ElfGroup[] =>
    rucksacks
        .reduce((accumulator: ElfGroup[], rucksack) => {
                if (!accumulator.length || accumulator[accumulator.length - 1].rucksacks.length > 2) {
                    accumulator.push({
                        rucksacks: []
                    });
                }
                accumulator[accumulator.length - 1].rucksacks.push(rucksack);
                return accumulator;
            },
            []);

const findItemInCommon = (group: ElfGroup): string => {
    const firstRucksack = group.rucksacks[0];
    const otherRucksacks = group.rucksacks.slice(1);
    const itemsInCommon: string[] = firstRucksack.items
        .filter((item: string, index: number, list: string[]) => list.indexOf(item) === index)
        .filter((item: string) =>
            otherRucksacks
                .every((otherRucksack) => otherRucksack.items.includes(item)));

    if (itemsInCommon.length !== 1) {
        throw new Error('Expected exactly one item in common');
    }
    return itemsInCommon[0];
};

const partTwo = (fileName: string) =>
    sumNumbers(
        groupRucksacks(
            splitRucksacks(
                readFile(fileName)
            )
        )
            .map(group => findItemInCommon(group))
            .map(groupItem => priorityFromItem(groupItem))
    );

console.log(partOne('day-03-example.txt'));

console.log(partOne('day-03-input.txt'));

console.log(partTwo('day-03-example.txt'));

console.log(partTwo('day-03-input.txt'));
