import * as fs from 'fs';

type ElfCalories = number[];
type ElfWithCalories = {
    elfIndex: number;
    calories: ElfCalories;
    caloryCount: number;
}
type ElfRanking = {
    top: ElfWithCalories[];
    minCalories?: number;
    maxCalories?: number;
}

const readFile = (fileName: string): string[] => {
    return fs.readFileSync(fileName)
        .toString('utf-8')
        .split('\r\n');
};

const elfCalories = (fileLines: string[]): ElfCalories[] =>
    fileLines
        .reduce((collector: any,
                 line: string,
                 index: number,
                 originalList: string[]): string[][] => {
            if (line.trim()) {
                const last = [
                    ...collector[collector.length - 1],
                    line,
                ];

                return [
                    ...collector.slice(0, collector.length - 1),
                    last,
                ];
            }

            if (index === originalList.length - 1) {
                return collector;
            }

            return [
                ...collector,
                [],
            ];
        }, [[]])
        .map((elfCaloryList: string[]): ElfCalories => elfCaloryList.map(caloryCount => parseInt(caloryCount)));

const elfCalorySummary = (elfIndex: number,
                          calories: ElfCalories): ElfWithCalories => {
    return {
        elfIndex,
        calories,
        caloryCount: calories.reduce((a, b) => a + b, 0),
    };
};

const aggregateElves = (calories: ElfCalories[]): ElfWithCalories[] =>
    calories
        .map((elfCalories: ElfCalories, index: number): ElfWithCalories => elfCalorySummary(index, elfCalories));

const getMostCaloriesElf = (elves: ElfWithCalories[]): ElfWithCalories =>
    elves
        .reduce((a, b) => {
            if (a.caloryCount > b.caloryCount) {
                return a;
            }
            return b;
        });

const elfRankingFrom = (elves: ElfWithCalories[]): ElfRanking => {
    return {
        top: elves,
        minCalories: elves
            .map((elf: ElfWithCalories) => elf.caloryCount)
            .reduce((countA: number, countB: number) => Math.min(countA, countB)),
        maxCalories: elves
            .map((elf: ElfWithCalories) => elf.caloryCount)
            .reduce((countA: number, countB: number) => Math.max(countA, countB)),
    };
};

const removeLowerThan = (threshold: number,
                         elves: ElfWithCalories[]) => {
    const lowestIndex = elves.findIndex((elf) => elf.caloryCount <= threshold);
    if (lowestIndex >= 0) {
        return elves.filter((_, index: number) => index !== lowestIndex);
    }
    return elves;
};

const getTopCaloryElves = (elves: ElfWithCalories[],
                           topN: number): ElfRanking =>
    elves
        .reduce((collector: ElfRanking, elf: ElfWithCalories): ElfRanking => {
            if (collector.top.length < topN) {
                return elfRankingFrom([...collector.top, elf]);
            }

            if (collector.minCalories && elf.caloryCount > collector.minCalories) {
                return elfRankingFrom([
                    ...removeLowerThan(collector.minCalories, collector.top),
                    elf
                ]);
            }

            return collector;
        }, {
            top: [],
        });


const partOne = (fileName: string) =>
    getMostCaloriesElf(
        aggregateElves(
            elfCalories(
                readFile(fileName)
            )
        )
    );

const partTwo = (fileName: string) =>
    getTopCaloryElves(
        aggregateElves(
            elfCalories(
                readFile(fileName)
            )
        ),
        3
    )
        .top
        .map(elf => elf.caloryCount)
        .reduce((a, b) => a + b, 0);

console.log('--- Part 1 ---');

console.log(partOne('day-01-input-example.txt'));

console.log(partOne('day-01-input.txt'));

console.log('--- Part 2 ---');

console.log(partTwo('day-01-input-example.txt'));

console.log(partTwo('day-01-input.txt'));
