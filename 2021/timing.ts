const disableOutput = () => {
    const consoleLogMethod = console.log;
    console.log = () => {
    };
    return () => console.log = consoleLogMethod;
}

const days = 7;
const iterations = 10;
const timings = [];

for (let day = 1; day <= days; day++) {
    process.stdout.write('Day ' + day);
    let totalTime = 0;
    for (let i = 0; i < iterations; i++) {
        const restoreConsoleLog = disableOutput();
        const file = './day-0' + day + '.ts';
        const start = +new Date();
        require(file);
        totalTime += +new Date() - start;
        delete require.cache[require.resolve(file)]
        restoreConsoleLog();
        if (i % Math.round(iterations / 20) === 0) {
            process.stdout.write('.');
        }
    }

    const average = (totalTime / iterations);
    timings.push(average);

    console.log(average + ' ms');
}

console.log('On average: ' + (timings.reduce((a, b) => a + b, 0) / timings.length));
