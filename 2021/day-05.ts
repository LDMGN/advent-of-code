import * as fs from 'fs';

interface Point {
    x: number;
    y: number;
}

interface Line {
    points: Point[];
}

const pointsFrom = (line: string): Point[] => {
    const points = line.split(' -> ');
    if (points.length !== 2) {
        throw new Error('Discovered ' + points.length + ' on one line ¯\\_(ツ)_/¯');
    }

    return points
        .map((point: string): Point => {
            const coordinates = point
                .split(',')
                .map(coordinate => parseInt(coordinate, 10));

            if (coordinates.length !== 2) {
                throw new Error('Discovered ' + coordinates.length + ' in one point ¯\\_(ツ)_/¯');
            }

            return {
                x: coordinates[0],
                y: coordinates[1],
            }
        });
}

const interpolate = (points: Point[]): Point[] => {
    const xEqual = points[0].x === points[1].x;
    const yEqual = points[0].y === points[1].y;

    if (!xEqual && !yEqual) {
        return [];
    }

    const pointFromValues = xEqual
        ? (fixed: number, variable: number) => ({
            x: fixed,
            y: variable,
        })
        : (fixed: number, variable: number) => ({
            x: variable,
            y: fixed,
        });

    const variableCoordinate: (_: Point) => number = xEqual
        ? (p) => p.y
        : (p) => p.x;
    const fixedCoordinate: (_: Point) => number = xEqual
        ? (p) => p.x
        : (p) => p.y;

    const length = Math.abs(variableCoordinate(points[0]) - variableCoordinate(points[1]));
    const start = Math.min(variableCoordinate(points[0]), variableCoordinate(points[1]));
    return Array.from({length: length + 1})
        .map((_, index: number) => pointFromValues(fixedCoordinate(points[0]), start + index));
}

const interpolateDiagonal = (points: Point[]): Point[] => {
    const xDiff = Math.abs(points[0].x - points[1].x);
    const xStart = points[0].x;
    const yDiff = Math.abs(points[0].y - points[1].y);
    const yStart = points[0].y;

    const length = Math.max(xDiff, yDiff);

    const xDelta = (points[1].x - points[0].x) / length;
    const yDelta = (points[1].y - points[0].y) / length;

    return Array.from({length: length + 1})
        .map((_, index: number) => {
            return {
                x: xStart + index * xDelta,
                y: yStart + index * yDelta,
            }
        });
}

const arePointsEqual = (pointA: Point, pointB: Point) => pointA.x === pointB.x && pointA.y === pointB.y

const calculateIntersectionsCount = (fileName: string,
                                     interpolator: (points: Point[]) => Point[]) => {
    const textLines = fs.readFileSync(fileName)
        .toString('utf-8')
        .split('\r\n')
        .map(line => line.trim())
        .filter(line => line.length);

    const lines = textLines
        .map((line: string): Point[] => pointsFrom(line))
        .map((points: Point[]): Point[] => interpolator(points))
        .filter((points: Point[]): boolean => points.length > 0)
        .map((points: Point[]): Line => ({
            points,
        }));

    const allPoints = lines
        .reduce((a: Point[], b: Line) => {
            return [
                ...a,
                ...b.points,
            ];
        }, []);

    const duplicatePoints = allPoints
        .filter((point: Point, index: number, arr: Point[]) => {
            const foundAtIndex = arr.findIndex(otherPoint => arePointsEqual(point, otherPoint))
            return foundAtIndex !== -1 && foundAtIndex !== index;
        });

    const uniqueDuplicatePoints = duplicatePoints
        .filter((point: Point, index: number, arr: Point[]) => {
            const foundAtIndex = arr.findIndex(otherPoint => arePointsEqual(point, otherPoint))
            return foundAtIndex === index;
        });

    return uniqueDuplicatePoints.length;
}

console.log('Part 1 expected example output', 5);
console.log('Part 1 example output', calculateIntersectionsCount('day-05-input-example.txt', interpolate));
console.log('Part 1 output', calculateIntersectionsCount('day-05-input.txt', interpolate));
console.log('Part 2 expected example output', 12);
console.log('Part 2 example output', calculateIntersectionsCount('day-05-input-example.txt', interpolateDiagonal));
console.log('Part 2 output', calculateIntersectionsCount('day-05-input.txt', interpolateDiagonal));
