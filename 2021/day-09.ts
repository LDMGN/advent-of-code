import * as fs from 'fs';

interface Map {
    width: number;
    height: number;
    rows: number[][];
}

interface Basin {
    size: number;
}

const loadData = (fileName: string): Map => {
    const rows: number[][] = fs.readFileSync(fileName)
        .toString('utf-8')
        .split('\r\n')
        .map(line => line.trim())
        .filter(line => line.length)
        .map((row: string): string[] => row.split(''))
        .map((row: string[]) => row.map((height: string): number => parseInt(height, 10)));

    return {
        width: rows[0].length,
        height: rows.length,
        rows,
    };
};

interface Point {
    key?: string;
    x: number;
    y: number;
}

const above = (map: Map, position: Point): number | undefined => {
    if (position.y === 0) {
        return undefined;
    }
    return map.rows[position.y - 1][position.x];
}
const below = (map: Map, position: Point): number | undefined => {
    if (position.y === map.height - 1) {
        return undefined;
    }
    return map.rows[position.y + 1][position.x];
}
const left = (map: Map, position: Point): number | undefined => {
    if (position.x === 0) {
        return undefined;
    }
    return map.rows[position.y][position.x - 1];
}
const right = (map: Map, position: Point): number | undefined => {
    if (position.x === map.width - 1) {
        return undefined;
    }
    return map.rows[position.y][position.x + 1];
}

const isHigherThen = (otherHeight: number | undefined, height: number) =>
    otherHeight === undefined
        ? true
        : otherHeight > height

const isLowest = (map: Map, position: Point): boolean => {
    const point = map.rows[position.y][position.x];
    return isHigherThen(above(map, position), point)
        && isHigherThen(below(map, position), point)
        && isHigherThen(left(map, position), point)
        && isHigherThen(right(map, position), point);
}

const lowPointCoordinates = (map: Map): Point[] =>
    Array.from({length: map.width * map.height})
        .map((_, index): Point => {
            return {
                x: index % map.width,
                y: Math.floor(index / map.width),
            }
        })
        .filter(position => isLowest(map, position));

const lowPointsValues = (map: Map): number[] =>
    lowPointCoordinates(map)
        .map((position: Point): number => map.rows[position.y][position.x]);

const riskLevels = (points: number[]): number[] =>
    points
        .map(point => point + 1);

const sum = (points: number[]): number => points.reduce((a, b) => a + b, 0);

const withKey = (p: Point): Point => ({
    ...p,
    key: p.x + '-' + p.y,
});

const extend = (point: Point,
                map: Map,
                walker: (_: Point) => Point,
                visited: string[]): Point[] => {
    const newPoint = withKey(walker(point));
    if (newPoint.x === -1 || newPoint.x === map.width || newPoint.y === -1 || newPoint.y === map.height) {
        return [];
    }

    const value = map.rows[newPoint.y][newPoint.x];
    if (value === 9) {
        return [];
    }

    if (visited.includes(newPoint.key)) {
        return [];
    }

    return [
        newPoint,
        ...extend(newPoint, map, walker, [...visited, newPoint.key]),
    ];
};

const walkBasin = (map: Map, point: Point, visitedKeys: string[]): string[] => {
    const newPoints: Point[] = [
        ...extend(withKey(point), map, (p) => ({x: p.x, y: p.y + 1}), visitedKeys),
        ...extend(withKey(point), map, (p) => ({x: p.x, y: p.y - 1}), visitedKeys),
        ...extend(withKey(point), map, (p) => ({x: p.x + 1, y: p.y}), visitedKeys),
        ...extend(withKey(point), map, (p) => ({x: p.x - 1, y: p.y}), visitedKeys),
    ];

    const newVisited = [
        ...visitedKeys,
        ...newPoints
            .map(point => point.key)
    ];

    if (visitedKeys.length === newVisited.length) {
        return newVisited;
    }

    return newPoints
        .map(newPoint => walkBasin(map, newPoint, newVisited))
        .reduce((a, b) => [...a, ...b], []);
}

const startBasinWalk = (map: Map, point: Point): number => {
    const lowPoint = withKey(point);
    return new Set(walkBasin(map, lowPoint, [lowPoint.key])).size;
}

const basins = (map: Map): Basin[] => {
    const lows: Point[] = lowPointCoordinates(map);
    return lows
        .map((lowPoint: Point): number => startBasinWalk(map, lowPoint))
        .map((size: number): Basin => ({
            size,
        }));
}

const mapToSize = (basins: Basin[]): number[] => basins.map(basin => basin.size);

const sortBySize = (basins: Basin[]): Basin[] => [...basins].sort((a, b) => b.size - a.size)

const take = <T>(n: number, list: T[]): T[] => list.slice(0, n);

const multiply = (points: number[]): number => points.reduce((a, b) => a * b, 1);

console.log('Day 9 part 1 example expected result', 15);
console.log('Day 9 part 1 example result', sum(riskLevels(lowPointsValues(loadData('day-09-input-example.txt')))));
console.log('Day 9 part 1 result', sum(riskLevels(lowPointsValues(loadData('day-09-input.txt')))));
console.log('Day 9 part 2 example expected result', 1134);
console.log('Day 9 part 2 example result', multiply(mapToSize(take(3, sortBySize(basins(loadData('day-09-input-example.txt')))))));
console.log('Day 9 part 2 result', multiply(mapToSize(take(3, sortBySize(basins(loadData('day-09-input.txt')))))));
