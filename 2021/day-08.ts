import * as fs from 'fs';

type SegmentElement = string;

type SignalPattern = SegmentElement[];

type DisplayDigit = string;

interface DisplayData {
    signalPatterns: SignalPattern[],
    digits: DisplayDigit[],
}

const getInput = (fileName: string): DisplayData[] => {
    return fs.readFileSync(fileName)
        .toString('utf-8')
        .split('\r\n')
        .map(line => line.trim())
        .filter(line => line.length)
        .map(line => {
            const parts = line.split(' | ');
            const signalPatterns = parts[0]
                .split(' ')
                .map(digit => {
                    const digits = digit.trim().split('');
                    digits.sort();
                    return digits;
                });
            const digits = parts[1]
                .split(' ')
                .map(digit => {
                    const digits = digit.trim().split('');
                    digits.sort();
                    return digits.join('');
                });
            return {
                signalPatterns,
                digits,
            }
        });
}

const whichNumber = (signals: string[]): number | undefined => {
    switch (signals.length) {
        case 2:
            return 1;
        case 3:
            return 7;
        case 4:
            return 4;
        case 7:
            return 8;
    }
    return undefined;
};

const calculateEasyDigits = (lines: DisplayData[]) => {
    return lines
        .map(line => line.digits)
        .reduce((a, b) => [...a, ...b], [])
        .map((digits: DisplayDigit) => digits.split(''))
        .map((digit) => whichNumber(digit))
        .filter(digit => digit !== undefined)
        .length;
};

const minus = (toSubstract: SignalPattern): (_: SegmentElement) => boolean => {
    return (toFilter: SegmentElement): boolean => {
        return !toSubstract.includes(toFilter);
    };
};

type SignalPatternFilter = (_: SignalPattern) => boolean;

const isEqual = (a: SignalPattern): SignalPatternFilter => {
    return (b: SignalPattern): boolean => {
        return a.join('') === b.join('');
    };
};

const not = (filter: SignalPatternFilter): SignalPatternFilter => {
    return (a: SignalPattern): boolean => {
        return !filter(a);
    };
};

interface WiringDiagram {
    [key: string]: number;
}

const mapping = (line: DisplayData): WiringDiagram => {
    /* Numbers. */
    const one: SignalPattern = line.signalPatterns
        .find(pattern => pattern.length === 2);
    const four: SignalPattern = line.signalPatterns
        .find(pattern => pattern.length === 4);
    const seven: SignalPattern = line.signalPatterns
        .find(pattern => pattern.length === 3);
    const eight: SignalPattern = line.signalPatterns
        .find(pattern => pattern.length === 7);

    const three: SignalPattern = line.signalPatterns
        .filter((pattern) => pattern.length === 5)
        .find((potentialThree: SignalPattern) => seven.every((segment: string) => potentialThree.includes(segment)));

    /* Individual segments. */
    const bottomSegment = three
        .filter(minus(seven))
        .filter(minus(four))
        .find(() => true);
    const topLeftSegment = four
        .filter(minus(three))
        .find(() => true);
    const middleSegment = three
        .filter(minus(seven))
        .find(minus([bottomSegment]));

    /* Calculate remaining numbers. */
    const five = line.signalPatterns
        .filter(pattern => pattern.length === 5)
        .find(pattern => pattern.includes(topLeftSegment));

    const two = line.signalPatterns
        .filter(pattern => pattern.length === 5)
        .filter(not(isEqual(five)))
        .find(not(isEqual(three)));
    const nine = line.signalPatterns
        .filter(pattern => pattern.length === 6)
        .filter(pattern => pattern.includes(middleSegment))
        .find(pattern => one.every(segment => pattern.includes(segment)));
    const zero = line.signalPatterns
        .filter(pattern => pattern.length === 6)
        .filter(not(isEqual(nine)))
        .find(pattern => one.every(segment => pattern.includes(segment)));
    const six = line.signalPatterns
        .filter(pattern => pattern.length === 6)
        .filter(not(isEqual(nine)))
        .find(not(isEqual(zero)));

    return {
        [one.join('')]: 1,
        [two.join('')]: 2,
        [three.join('')]: 3,
        [four.join('')]: 4,
        [five.join('')]: 5,
        [six.join('')]: 6,
        [seven.join('')]: 7,
        [eight.join('')]: 8,
        [nine.join('')]: 9,
        [zero.join('')]: 0,
    }
};

const deduceValue = (lines: DisplayData[]) => {
    return lines
        .map(line => {
            const wiring = mapping(line);
            return line.digits
                .map(digit => wiring[digit])
                .join('');
        })
        .map(foo => parseInt(foo, 10))
        .reduce((a, b) => a + b, 0);
};

console.log('Day 8 part 1 example expected result', 26);
console.log('Day 8 part 1 example result', calculateEasyDigits(getInput('day-08-input-example-2.txt')));
console.log('Day 8 part 1 result', calculateEasyDigits(getInput('day-08-input-part-1.txt')));
console.log('Day 8 part 2 example expected result', 5353);
console.log('Day 8 part 2 example result', deduceValue(getInput('day-08-input-example.txt')));
console.log('Day 8 part 2 example 2 result', deduceValue(getInput('day-08-input-example-2.txt')));
console.log('Day 8 part 2 example 2 result', deduceValue(getInput('day-08-input-part-1.txt')));
