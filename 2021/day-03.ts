import * as fs from 'fs';

interface BitCount {
    0: 0,
    1: 0,
}

const getBitCountsFromLines = (lines: string[]): BitCount[] => {
    if (!lines.length) {
        return [];
    }

    const lineLengths = new Set(lines
        .map(line => line.length));
    if (lineLengths.size > 1) {
        throw new Error('Cannot calculate bit sets from unevenly sized lines');
    }

    return Array.from({length: lines[0].length})
        .map((_, i) => i)
        .map((index: number) => lines.map(line => line.charAt(index)))
        .map((bits: string[]) => {
            const startCount: BitCount = {0: 0, 1: 0};
            return bits.reduce((a: BitCount, b) => {
                return {
                    ...a,
                    [b]: a[b] + 1,
                }
            }, startCount)
        });
};

const getBitsFromCounts = (counts: BitCount[],
                           bitMapper: (_: BitCount) => number): string => {
    return counts
        .map(count => bitMapper(count))
        .reduce((a, b) => a + b, '');
};

const bitFromHighestCount = (count: BitCount): number => {
    if (count['0'] > count['1']) {
        return 0;
    }
    return 1;
}
const bitFromLowestCount = (count: BitCount): number => {
    if (count['0'] > count['1']) {
        return 1;
    }
    return 0;
}

const submarinePowerConsumption = (fileName: string): number => {
    const example = fs.readFileSync(fileName);
    const lines = example.toString('utf-8').split('\r\n').filter(line => !!line);

    const bitCounts = getBitCountsFromLines(lines);

    const gammaRate = parseInt(getBitsFromCounts(bitCounts, bitFromHighestCount), 2);
    const epsilonRate = parseInt(getBitsFromCounts(bitCounts, bitFromLowestCount), 2);
    return gammaRate * epsilonRate;
}

const filterOut = (lines: string[],
                   bitCriteria: (_: BitCount) => number,
                   bitPosition: number = 0): string => {
    if (lines.length === 1) {
        return lines[0];
    }

    const bitCounts = getBitCountsFromLines(lines);
    const criteriaValue: number = bitCriteria(bitCounts[bitPosition]);
    const filteredLines = lines
        .filter((a: string) => parseInt(a.charAt(bitPosition)) === criteriaValue);
    return filterOut(filteredLines, bitCriteria, bitPosition + 1);
}

const submarineLifeSupportRating = (fileName: string): number => {
    const example = fs.readFileSync(fileName);
    const lines = example.toString('utf-8').split('\r\n').filter(line => !!line);

    const oxygenGeneratorRating = parseInt(filterOut(lines, bitFromHighestCount), 2);
    const co2ScrubberRating = parseInt(filterOut(lines, bitFromLowestCount), 2);

    return oxygenGeneratorRating * co2ScrubberRating;
};

console.log('Part 1 example expected power consumption', 198);
console.log('Part 1 example', submarinePowerConsumption('day-03-input-example.txt'));
console.log('Part 1', submarinePowerConsumption('day-03-input.txt'));
console.log('Part 2 example', submarineLifeSupportRating('day-03-input-example.txt'));
console.log('Part 2', submarineLifeSupportRating('day-03-input.txt'));
