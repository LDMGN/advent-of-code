import * as fs from 'fs';

const loadDataSet = (fileName: string) =>
    fs.readFileSync(fileName)
        .toString('utf-8')
        .split(',')
        .map(position => position.trim())
        .filter(position => position.length)
        .map(position => parseInt(position, 10))

const calculateFuelConsumptionToMoveTo = (positions: number[], desiredPosition: number): number => {
    return positions
        .map(position => Math.abs(position - desiredPosition))
        .reduce((a, b) => a + b, 0);
}

const calculateIncrementalFuelConsumptionToMoveTo = (positions: number[], desiredPosition: number): number => {
    return positions
        .map(position => {
            const steps = Math.abs(position - desiredPosition);
            let stepCost = 0;
            for (let step = 0; step <= steps; step++) {
                stepCost += step;
            }
            return stepCost;
        })
        .reduce((a, b) => a + b, 0);
}

const calculateCheapestPosition = (positions: number[],
                                   calculator: (positions: number[], desiredPosition: number) => number) => {
    const minPosition = positions.reduce((a, b) => Math.min(a, b));
    const maxPosition = positions.reduce((a, b) => Math.max(a, b));
    const positionRange = maxPosition - minPosition + 1;

    const possiblePositions = Array.from({length: positionRange})
        .map((_, index: number) => {
            return minPosition + index;
        });

    return possiblePositions
        .map(position => calculator(positions, position))
        .reduce((a, b) => Math.min(a, b));
}

console.log('Part 1 example', 41, calculateFuelConsumptionToMoveTo(loadDataSet('day-07-input-example.txt'), 1));
console.log('Part 1 example', 39, calculateFuelConsumptionToMoveTo(loadDataSet('day-07-input-example.txt'), 3));
console.log('Part 1 example', 71, calculateFuelConsumptionToMoveTo(loadDataSet('day-07-input-example.txt'), 10));
console.log('Part 1 example', 37, calculateCheapestPosition(loadDataSet('day-07-input-example.txt'), calculateFuelConsumptionToMoveTo));
console.log('Part 1', calculateCheapestPosition(loadDataSet('day-07-input.txt'), calculateFuelConsumptionToMoveTo));
console.log('Part 2 example', 206, calculateIncrementalFuelConsumptionToMoveTo(loadDataSet('day-07-input-example.txt'), 2));
console.log('Part 2 example', 168, calculateCheapestPosition(loadDataSet('day-07-input-example.txt'), calculateIncrementalFuelConsumptionToMoveTo));
console.log('Part 2', calculateCheapestPosition(loadDataSet('day-07-input.txt'), calculateIncrementalFuelConsumptionToMoveTo));
