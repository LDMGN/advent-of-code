import * as fs from 'fs';

type Population = number[];
const getPopulation = (fileName: string): Population => {
    return fs.readFileSync(fileName)
        .toString('utf-8')
        .split(',')
        .map(counter => parseInt(counter, 10));
}

const offSpringAfterDays = (fishCounter: number, days: number): number => {
    let day = days - fishCounter;
    let fishOffspring = 0;
    while (day > 0) {
        fishOffspring += offSpringAfterDays(8, day - 1);
        day -= 7;
    }
    return 1 + fishOffspring;
}

const populationAfterDays = (population: Population, days: number): number => {
    const populationHistogram = population
        .reduce((accumulator, value) => ({
            ...accumulator,
            [value]: accumulator[value] + 1,
        }), {
            0: 0,
            1: 0,
            2: 0,
            3: 0,
            4: 0,
            5: 0,
            6: 0,
        });

    const uniqueCounters = Array.from(new Set(population));
    const offSpringCount = uniqueCounters
        .map(counter => ({
            counter,
            offspring: offSpringAfterDays(counter, days),
        }))
        .reduce((accumulator, value) => ({
            ...accumulator,
            [value.counter]: value.offspring,
        }), {});

    return uniqueCounters
        .map(counter => populationHistogram[counter] * offSpringCount[counter])
        .reduce((a, b) => a + b, 0);
}

console.log('Part 1 expected example result', 5934);
console.log('Part 1 example result', populationAfterDays(getPopulation('day-06-input-example.txt'), 80));
console.log('Part 1 result', populationAfterDays(getPopulation('day-06-input.txt'), 80));
console.log('Part 2 expected example result', 26984457539);
console.log('Part 2 example result', populationAfterDays(getPopulation('day-06-input-example.txt'), 256));
console.log('Part 2 result', populationAfterDays(getPopulation('day-06-input.txt'), 256));
