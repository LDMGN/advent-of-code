import * as fs from 'fs';

interface BingoDataSet {
    numbers: number[];
    boards: BingoBoard[];
}

interface BingoRow {
    numbers: number[];
}

interface BingoColumn extends BingoRow {
}

interface BingoBoard {
    rows: BingoRow[];
    columns: BingoColumn[];
}

interface WinningBingoBoard extends BingoBoard {
    score: number;
}

const unmarkedNumbersFromBoard = (board: BingoBoard,
                                  numbers: number[]): number[] => {
    return board.rows
        .map((row: BingoRow): number[] => row.numbers.filter(number => !numbers.includes(number)))
        .reduce((a, b) => {
            return [
                ...a,
                ...b,
            ]
        }, []);
}

const boardScore = (board: BingoBoard,
                    numbers: number[],
                    lastNumber: number) => {
    const unmarkedNumbers = unmarkedNumbersFromBoard(board, numbers);
    const unmarkedSum = unmarkedNumbers.reduce((a: number, b: number) => a + b, 0);
    return unmarkedSum * lastNumber;
}

const boardWins = (board: BingoBoard,
                   numbers: number[]): undefined | WinningBingoBoard => {
    if (!numbers.length) {
        return undefined;
    }

    const rowMatches = board.rows.some(row => row.numbers.every(rowNumber => numbers.includes(rowNumber)));
    const columnMatches = board.columns.some(column => column.numbers.every(columnNumber => numbers.includes(columnNumber)));
    if (rowMatches || columnMatches) {
        const lastNumber = numbers[numbers.length - 1];
        const score = boardScore(board, numbers, lastNumber);
        return {
            ...board,
            score,
        }
    }
    return undefined;
}

const getWinningBoard = (boards: BingoBoard[],
                         numbers: number[]): undefined | WinningBingoBoard => {
    const numbersToAnounce = Array.from({length: numbers.length})
        .map((_, index: number): number => index)
        .map((numberOfNumbers: number): number[] => numbers.slice(0, numberOfNumbers));

    for (let i = 0; i < numbersToAnounce.length; i++) {
        const roundNumbers = numbersToAnounce[i];
        for (let boardIndex = 0; boardIndex < boards.length; boardIndex++) {
            const winningBoard = boardWins(boards[boardIndex], roundNumbers);
            if (winningBoard) {
                return winningBoard;
            }
        }
    }
    return undefined;
}

const boardsEqual = (boardA: BingoBoard, boardB: BingoBoard) => {
    for (let y = 0; y < boardA.rows.length; y++) {
        for (let x = 0; x < boardA.rows[y].numbers.length; x++) {
            if (boardA.rows[y].numbers[x] !== boardB.rows[y].numbers[x]) {
                return false;
            }
        }
    }
    return true;
}

const getLastWinningBoard = (boards: BingoBoard[],
                             numbers: number[]): undefined | WinningBingoBoard => {
    const firstWinningBoard = getWinningBoard(boards, numbers);
    if (!firstWinningBoard) {
        throw new Error('Not all boards win ¯\\_(ツ)_/¯');
    }

    if (boards.length === 1) {
        return firstWinningBoard;
    }

    const winningIndex = boards.findIndex(board => boardsEqual(board, firstWinningBoard));
    const newBoards = [
        ...boards,
    ];
    newBoards.splice(winningIndex, 1);
    return getLastWinningBoard(newBoards, numbers);
}

const columnsFromRows = (rows: BingoRow[]): BingoColumn[] => {
    const gridWidth = rows[0].numbers.length;
    const gridHeight = rows.length;
    const columns: BingoColumn[] = Array.from({length: gridWidth})
        .map((): BingoColumn => {
            return {
                numbers: Array.from({length: gridHeight}),
            }
        });

    for (let x = 0; x < gridWidth; x++) {
        for (let y = 0; y < gridWidth; y++) {
            columns[x].numbers[y] = rows[y].numbers[x];
        }
    }

    return columns;
}

const loadBingoData = (fileName: string): BingoDataSet => {
    const example = fs.readFileSync(fileName);
    const lines = example.toString('utf-8')
        .split('\r\n');
    const numbers = lines[0]
        .split(',')
        .map(number => parseInt(number, 10));

    const boards: BingoBoard[] = [];
    let newRows: BingoRow[] = [];
    for (let i = 1; i < lines.length; i++) {
        const currentLine = lines[i].trim();
        if (currentLine.length === 0) {
            if (newRows.length) {
                const newBoard = {
                    rows: newRows,
                    columns: columnsFromRows(newRows),
                };
                boards.push(newBoard);
                newRows = [];
            }
            continue;
        }

        const rowNumbers: number[] = currentLine
            .split(' ')
            .map((number: string) => number.trim())
            .filter((number: string) => number.length > 0)
            .map((number: string) => parseInt(number, 10));

        newRows.push({
            numbers: rowNumbers,
        });
    }

    return {
        numbers,
        boards,
    }
}

const exampleData = loadBingoData('day-04-input-example.txt');
const data = loadBingoData('day-04-input.txt');

const expectedExampleScore = 4512;
console.log('Part 1 expected example score', expectedExampleScore);
console.log('Part 1 example score', getWinningBoard(exampleData.boards, exampleData.numbers)?.score);
console.log('Part 1 score', getWinningBoard(data.boards, data.numbers)?.score);

const expectedExampleScorePart2 = 1924;
console.log('Part 2 expected example score', expectedExampleScorePart2);
console.log('Part 2 example score', getLastWinningBoard(exampleData.boards, exampleData.numbers)?.score);
console.log('Part 2 score', getLastWinningBoard(data.boards, data.numbers)?.score);
