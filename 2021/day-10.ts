import * as fs from 'fs';

const roundOpen = '(';
const bracketOpen = '[';
const curlyOpen = '{';
const greaterThanOpen = '<';

enum OpenToken {
    roundOpen,
    bracketOpen,
    crulyOpen,
    greaterThanOpen,
}

const roundClose = ')';
const bracketClose = ']';
const curlyClose = '}';
const greaterThanClose = '>';

enum CloseToken {
    roundClose,
    bracketClose,
    crulyClose,
    greaterThanClose,
}

const chunkPairs = {
    [roundOpen]: roundClose,
    [bracketOpen]: bracketClose,
    [curlyOpen]: curlyClose,
    [greaterThanOpen]: greaterThanClose,
};

const syntaxCheckScores = {
    [roundClose]: 3,
    [bracketClose]: 57,
    [curlyClose]: 1197,
    [greaterThanClose]: 25137,
}

const autocompleteScores = {
    [roundClose]: 1,
    [bracketClose]: 2,
    [curlyClose]: 3,
    [greaterThanClose]: 4,
}

interface Chunk {
    openToken?: OpenToken;
    closeToken?: CloseToken;
    chunks: Chunk[];

    otherToken?: string;
}

const getLines = (fileName: string) =>
    fs.readFileSync(fileName)
        .toString('utf-8')
        .split('\r\n')
        .map(line => line.trim())
        .filter(line => line.length);

const isOpenCharacter = (character: unknown): character is OpenToken =>
    character === roundOpen
    || character === bracketOpen
    || character === curlyOpen
    || character === greaterThanOpen;

const isCloseCharacter = (character: unknown): character is CloseToken =>
    character === roundClose
    || character === bracketClose
    || character === curlyClose
    || character === greaterThanClose;

interface ParseResult {
    chunks: Chunk[];
    lineRemainder: string;
}

const parse = (line: string): ParseResult => {
    let lineRemainder = line;
    let openChunk: Chunk | undefined;
    const discoveredChunks: Chunk[] = [];
    while (lineRemainder.length) {
        const nextCharacter = lineRemainder[0];
        if (isOpenCharacter(nextCharacter)) {
            if (openChunk) {
                const subParseResult = parse(lineRemainder);
                openChunk.chunks = subParseResult.chunks;
                lineRemainder = subParseResult.lineRemainder;
            } else {
                openChunk = {
                    openToken: nextCharacter,
                    chunks: [],
                };
                lineRemainder = lineRemainder.substring(1);
            }
        } else if (isCloseCharacter(nextCharacter)) {
            if (openChunk) {
                if (chunkPairs[openChunk.openToken] === nextCharacter) {
                    openChunk.closeToken = nextCharacter;
                } else {
                    openChunk.otherToken = nextCharacter;
                }
                discoveredChunks.push(openChunk);
                openChunk = undefined;
                lineRemainder = lineRemainder.substring(1);
            } else {
                break;
            }
        } else {
            if (openChunk) {
                openChunk.otherToken = nextCharacter;
                discoveredChunks.push(openChunk);
            } else {
                discoveredChunks.push({otherToken: nextCharacter, chunks: []});
            }
            return {
                chunks: discoveredChunks,
                lineRemainder,
            }
        }
    }
    if (openChunk) {
        discoveredChunks.push(openChunk);
    }

    return {
        chunks: discoveredChunks,
        lineRemainder,
    }
}

interface LineResult {
    line: string;
    lineRemainder: string;
    chunks: Chunk[];
}

const getChunks = (lines: string[]): LineResult[] => {
    return lines
        .map(line => {
            const parseResult = parse(line);
            return ({
                line,
                lineRemainder: parseResult.lineRemainder,
                chunks: parseResult.chunks,
            });
        });
}

const anyIncorrectChunks = (chunks: Chunk[]) =>
    chunks.some(chunk => !!chunk.otherToken || anyIncorrectChunks(chunk.chunks));

const isIncorrectLine = (line: LineResult) =>
    line.lineRemainder.length || anyIncorrectChunks(line.chunks);

const getIncorrect = (lines: LineResult[]): LineResult[] =>
    lines.filter(line => isIncorrectLine(line));

const flattenChunks = (chunks: Chunk[]): Chunk[] => {
    return chunks
        .map(chunk => {
            return [
                {
                    ...chunk,
                    chunks: [],
                },
                ...flattenChunks(chunk.chunks),
            ];
        })
        .reduce((a, b) => [...a, ...b], []);
}

const withOtherTokens = (chunks: Chunk[]): Chunk[] =>
    chunks.filter(chunk => !!chunk.otherToken);

const first = <T>(list: T[]): T | undefined => list.find(() => true);

const syntaxErrorScore = (lineResults: LineResult[]) => {
    return getIncorrect(lineResults)
        .map(line => first(withOtherTokens(flattenChunks(line.chunks))).otherToken)
        .map(otherToken => syntaxCheckScores[otherToken])
        .reduce((a, b) => a + b, 0);
}

const getIncomplete = (lines: LineResult[]) => lines.filter(line => !isIncorrectLine(line));

const completeAllChunks = (lines: LineResult[]): string[] =>
    lines
        .map(line => flattenChunks(line.chunks))
        .map(chunks => chunks.filter(chunk => !chunk.closeToken))
        .map(chunks => chunks.map(chunk => chunkPairs[chunk.openToken]))
        .map(chunks => [...chunks,].reverse())
        .map(chunks => chunks.map(a => '' + a).join(''));

const calculateAutocompleteScores = (sequences: string[]): number[] =>
    sequences.map((sequence: string) =>
        sequence.split('')
            .map(character => autocompleteScores[character])
            .reduce((a, b) => a * 5 + b, 0))

const autocompleteWinner = (scores: number[]): number => {
    if (!scores.length) {
        return 0;
    }
    const sortedScores = [...scores].sort((a, b) => b - a);
    return sortedScores[Math.floor(sortedScores.length / 2)];
};

console.log('Expect 0 incorrect in examples 1 (correct)', getIncorrect(getChunks(getLines('day-10-input-example-1.txt'))));
console.log('Expect 0 incorrect in examples 2 (incomplete)', getIncorrect(getChunks(getLines('day-10-input-example-2.txt'))));
console.log('Expected example result', 26397);
console.log('Example result', syntaxErrorScore(getChunks(getLines('day-10-input-example-3.txt'))));
console.log('Part 1 result', syntaxErrorScore(getChunks(getLines('day-10-input.txt'))));
console.log('Part 2 expected example result', 288957);
console.log('Part 2 example result', autocompleteWinner(calculateAutocompleteScores(completeAllChunks(getIncomplete(getChunks(getLines('day-10-input-example-3.txt')))))));
console.log('Part 2 example result', autocompleteWinner(calculateAutocompleteScores(completeAllChunks(getIncomplete(getChunks(getLines('day-10-input.txt')))))));
