import {
    example,
    exampleExpectedResultPart1,
    exampleExpectedResultPart2,
    puzzleInput,
} from './day-01-input';

const part1 = (measurements: number[]) =>
    measurements
        .filter((value, index, array) => index !== 0 && value > array[index - 1])
        .length;

const part2 = (measurements: number[]) =>
    measurements
        .filter((value, index, array) => {
            if (index < 3) {
                return false;
            }
            const window = array[index - 2] + array[index - 1] + array[index];
            const lastWindow = array[index - 3] + array[index - 2] + array[index - 1];
            return window > lastWindow;
        })
        .length;

const exampleResultPart1 = part1(example);
if (exampleResultPart1 !== exampleExpectedResultPart1) {
    console.error(`${exampleResultPart1} not equal to expected result ${exampleExpectedResultPart1}`);
} else {
    console.log(exampleResultPart1);
}
console.log(part1(puzzleInput));

const exampleResultPart2 = part2(example);
if (exampleResultPart2 !== exampleExpectedResultPart2) {
    console.error(`${exampleResultPart2} not equal to expected result ${exampleExpectedResultPart2}`);
} else {
    console.log(exampleResultPart2);
}
console.log(part2(puzzleInput));
