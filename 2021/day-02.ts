import * as fs from 'fs';

interface SubmarineState {
    x: number;
    z: number;
    aim: number;
}

type SubmarineMovementManipulation = (state: SubmarineState) => SubmarineState;

type SubmarineMovementManipulationProducer = (x: number) => SubmarineMovementManipulation;

interface SubmarineMovementManipulationDictionary {
    [key: string]: SubmarineMovementManipulationProducer;
}

const submarine = (fileName: string, movements) => {
    const example = fs.readFileSync(fileName);
    const lines = example.toString('utf-8')
        .split('\r\n')
        .filter(line => !!line);

    const startPosition: SubmarineState = {
        x: 0,
        z: 0,
        aim: 0,
    };

    const finalPosition =
        lines
            .map((line: string): SubmarineMovementManipulation => {
                const lineParts = line.split(' ');
                const command = lineParts[0];
                const speed = parseInt(lineParts[1]);
                return movements[command](speed);
            })
            .reduce((a: SubmarineState, b: SubmarineMovementManipulation) => b(a), startPosition);

    return finalPosition.x * finalPosition.z;
};

const part1Movements: SubmarineMovementManipulationDictionary = {
    forward: (x: number) =>
        (state: SubmarineState) => ({
            ...state,
            x: state.x + x,
        }),
    down: (x: number) =>
        (state: SubmarineState) => ({
            ...state,
            z: state.z + x,
        }),
    up: (x: number) =>
        (state: SubmarineState) => ({
            ...state,
            z: state.z - x,
        }),
}

console.log('Part 1 Example', submarine('day-02-input-example.txt', part1Movements));
console.log('Part 1', submarine('day-02-input-part1.txt', part1Movements));

const part2Movements: SubmarineMovementManipulationDictionary = {
    forward: (x: number) =>
        (state: SubmarineState) => ({
            ...state,
            x: state.x + x,
            z: state.z + state.aim * x,
        }),
    down: (x: number) =>
        (state: SubmarineState) => ({
            ...state,
            aim: state.aim + x,
        }),
    up: (x: number) =>
        (state: SubmarineState) => ({
            ...state,
            aim: state.aim - x,
        }),
}
console.log('Part 2 Example', submarine('day-02-input-example.txt', part2Movements));
console.log('Part 2', submarine('day-02-input-part2.txt', part2Movements));
