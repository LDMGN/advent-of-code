import * as fs from 'fs';

/**
 * Input
 */
const day6Example = fs.readFileSync('./day-06-example.txt', 'utf-8');

const day6Input = fs.readFileSync('./day-06.txt', 'utf8');

/**
 * Part 1
 */
const parseSets = (input: string) => input
    .replace(/(\r)/gm, '')
    .split('\n\n');

const parseGroup = (group: string) => group
    .split('\n')
    .map(line => line.trim().split(''))
    .filter(line => line.length > 0);

const countItems = (items: any[]): number => items.reduce((a) => a + 1, 0);

const positiveAnswersPerGroup = (groups: string[][][]): number[] => groups
    .map((groups: string[][]): string[] => groups.reduce((a, b) => [...a, ...b], []))
    .map((group: string[]): string[] => group.filter((value: string, index: number, array: string[]) => array.findIndex(val => val === value) === index))
    .map((group: string[]): number => countItems(group));

const sumOfItems = (numbers: number[]) => numbers.reduce((a, b) => a + b, 0);

const exampleGroups = parseSets(day6Example).map(group => parseGroup(group));
const exampleCount = sumOfItems(positiveAnswersPerGroup(exampleGroups));
console.log('Example count', exampleCount);

const groups = parseSets(day6Input).map(group => parseGroup(group));
const part1Count = sumOfItems(positiveAnswersPerGroup(groups));
console.log('Part 1', part1Count);

/**
 * Part 2
 */
console.log('Any groups without positive answers', !!day6Input.match(/(\n\n\n)/gm));

const commonAnswersPerGroup = (groups: string[][][]): string[][] => groups
    .map((people: string[][]): string[] => {
        if (people.length === 1) {
            return people[0];
        }

        return people[0]
            .filter(answer => {
                return people
                    .slice(1)
                    .every(otherPersonAnswers => otherPersonAnswers.some(otherAnswer => otherAnswer === answer));
            });
    });

const countAnswersPerGroup = (answersPerGroup: string[][]): number[] => answersPerGroup.map((group: string[]): number => countItems(group));

const exampleCountPart2 = sumOfItems(countAnswersPerGroup(commonAnswersPerGroup(exampleGroups)));
console.log('Part 2 example count', exampleCountPart2);

const part2Count = sumOfItems(countAnswersPerGroup(commonAnswersPerGroup(groups)));
console.log('Part 2', part2Count)
